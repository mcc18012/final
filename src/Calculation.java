import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;

public class Calculation {

    Player p = new Player();
    private int realHitPoints;

    public void calcStats() {

        if (p.strength == 8 || p.strength == 9) {p.strengthMod -= 1;}
        else if (p.strength == 10 || p.strength == 11) {p.strengthMod = 0;}
        else if (p.strength == 12 || p.strength == 13) {p.strengthMod += 1;}
        else if (p.strength == 14 || p.strength == 15) {p.strengthMod += 2;}
        else if (p.strength == 16 || p.strength == 17) {p.strengthMod += 3;}
        else if (p.strength == 18 || p.strength == 19) {p.strengthMod += 4;}

        if (p.dexterity == 8 || p.dexterity == 9) {p.dexterityMod -= 1;}
        else if (p.dexterity == 10 || p.dexterity == 11) {p.dexterityMod = 0;}
        else if (p.dexterity == 12 || p.dexterity == 13) {p.dexterityMod += 1;}
        else if (p.dexterity == 14 || p.dexterity == 15) {p.dexterityMod += 2;}
        else if (p.dexterity == 16 || p.dexterity == 17) {p.dexterityMod += 3;}
        else if (p.dexterity == 18 || p.dexterity == 19) {p.dexterityMod += 4;}

        if (p.constitution == 8 || p.constitution == 9) {p.constitutionMod -= 1;}
        else if (p.constitution == 10 || p.constitution == 11) {p.constitutionMod = 0;}
        else if (p.constitution == 12 || p.constitution == 13) {p.constitutionMod += 1;}
        else if (p.constitution == 14 || p.constitution == 15) {p.constitutionMod += 2;}
        else if (p.constitution == 16 || p.constitution == 17) {p.constitutionMod += 3;}
        else if (p.constitution == 18 || p.constitution == 19) {p.constitutionMod += 4;}

        if (p.intelligence == 8 || p.intelligence == 9) {p.intelligenceMod -= 1;}
        else if (p.intelligence == 10 || p.intelligence == 11) {p.intelligenceMod = 0;}
        else if (p.intelligence == 12 || p.intelligence == 13) {p.intelligenceMod += 1;}
        else if (p.intelligence == 14 || p.intelligence == 15) {p.intelligenceMod += 2;}
        else if (p.intelligence == 16 || p.intelligence == 17) {p.intelligenceMod += 3;}
        else if (p.intelligence == 18 || p.intelligence == 19) {p.intelligenceMod += 4;}

        if (p.wisdom == 8 || p.wisdom == 9) {p.wisdomMod -= 1;}
        else if (p.wisdom == 10 || p.wisdom == 11) {p.wisdomMod = 0;}
        else if (p.wisdom == 12 || p.wisdom == 13) {p.wisdomMod += 1;}
        else if (p.wisdom == 14 || p.wisdom == 15) {p.wisdomMod += 2;}
        else if (p.wisdom == 16 || p.wisdom == 17) {p.wisdomMod += 3;}
        else if (p.wisdom == 18 || p.wisdom == 19) {p.wisdomMod += 4;}

        if (p.charisma == 8 || p.charisma == 9) {p.charismaMod -= 1;}
        else if (p.charisma == 10 || p.charisma == 11) {p.charismaMod = 0;}
        else if (p.charisma == 12 || p.charisma == 13) {p.charismaMod += 1;}
        else if (p.charisma == 14 || p.charisma == 15) {p.charismaMod += 2;}
        else if (p.charisma == 16 || p.charisma == 17) {p.charismaMod += 3;}
        else if (p.charisma == 18 || p.charisma == 19) {p.charismaMod += 4;}
    }

    public void raceCalc() {
        switch (p.race) {
            case "Dwarf":
                RacePage.Dwarf();
                break;
            case "Elf":
                RacePage.Elf();
                break;
            case "Gnome":
                RacePage.Gnome();
                break;
            case "Half-Elf":
                RacePage.Halfelf();
                break;
            case "Halfling":
                RacePage.Halfling();
                break;
            case "Half-Orc":
                RacePage.Halforc();
                break;
            case "Human":
                RacePage.Human();
                break;
        }

    }

    public void buildReader() throws Exception {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader("C:\\Users\\McCleve Family\\IdeaProjects\\RealFinal\\src\\BuildInfo.json"));
        JSONArray ja = (JSONArray)jsonObject.get("Builds");

        for (int i = 0; i < ja.size(); i++) {
            JSONObject jaloop = (JSONObject) ja.get(i);
            String build = (String) jaloop.get("Class");

            if (build.equals(Player.build)) {
                Long hitPoints = (Long) jaloop.get("HP");
                realHitPoints = hitPoints.intValue();
                Player.hitPoints = realHitPoints;
                Player.armorProf = (String) jaloop.get("Armor");
                Player.weaponProf = (JSONArray) jaloop.get("Weapons");
                Player.toolProf = (JSONArray) jaloop.get("Tools");
                Player.savingThrows = (JSONArray) jaloop.get("Saving Throws");
                Player.skillProf = (JSONArray) jaloop.get("Skills");
                Player.specials = (JSONArray) jaloop.get("Special");
                Long gp = (Long) jaloop.get("Gold");
                Player.buildGold = gp.intValue();
                }
            }
        }
    }