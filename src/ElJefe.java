import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "ElJefe", urlPatterns={"/Servlet"})
public class ElJefe extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Player p = new Player();
        Calculation c = new Calculation();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        p.name = request.getParameter("name");
        p.race = request.getParameter("race");
        p.build = request.getParameter("class1");
        p.strength = Integer.parseInt(request.getParameter("strength"));
        p.dexterity = Integer.parseInt(request.getParameter("dexterity"));
        p.constitution = Integer.parseInt(request.getParameter("constitution"));
        p.intelligence = Integer.parseInt(request.getParameter("intelligence"));
        p.wisdom = Integer.parseInt(request.getParameter("wisdom"));
        p.charisma = Integer.parseInt(request.getParameter("charisma"));

        try {
            p.compile();
        } catch (Exception e) {
            e.printStackTrace();
        }


        out.println("<h1>Your Character</h1>");
        out.println("<p>Character Name: " + p.name + "</p>");
        out.println("<p>Race: " + p.race + "</p>");
        out.println("<p>Class: " + p.build + "</p>");
        out.println("<p>Hit Points: " + p.hitPoints + "</p>");
        out.println("<h4>Statistics</h4>");
        out.println("<p>Strength "+ p.strength + " + (" + p.strengthMod + ")</p>");
        out.println("<p>Dexterity "+ p.dexterity + " + (" + p.dexterityMod + ")</p>");
        out.println("<p>Constitution: "+ p.constitution + " + (" + p.constitutionMod + ")</p>");
        out.println("<p>Intelligence: "+ p.intelligence + " + (" + p.intelligenceMod +")</p>");
        out.println("<p>Wisdom: "+ p.wisdom + " + (" + p.wisdomMod + ")</p>");
        out.println("<p>Charisma: "+ p.charisma + " + (" + p.charismaMod + ")</p>");
        out.println("<p>Armor proficiencies: " + p.armorProf + "</p>");
        out.println("<p>Weapon proficiencies: " + p.weaponProf + "</p>");
        out.println("<p>Tool proficiencies: " + p.toolProf + "</p>");
        out.println("<p>Saving throws: " + p.savingThrows + "</p>");
        out.println("<p>Skill proficiencies: " + p.skillProf + "</p>");
        out.println("<p>Special abilities: " + p.specials + "</p>");
        out.println("<p>Racial traits: " + p.traits + "</p>");
        out.println("<p>Starting gold: " + p.rolledGold + "</p>");
        out.println("<br><br>");
        out.println("<p>Congratulations! Your character is done!</p>");
        out.println("</body></html>");

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Player p = new Player();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        ArrayList array = new ArrayList();
        String name = request.getParameter("name");

        if ((name == null) || (name.equals(""))) {
            array.add("ERROR-NAME: Please enter player name.  ");
        }
        if ((p.race == null) || (p.race.equals(""))) {
            array.add("ERROR-RACE: Please select a race. It's not racist.  ");
        }

        if ((p.build == null) || (p.build.equals(""))) {
            array.add("ERROR-CLASS: Please select a class. It's not the bourgeousie.  ");
        }
        if (array.size() != 0) {
            out.println(array);
        }
    }

}
