import java.util.ArrayList;
import java.util.Collections;

public class Stuff2 implements Runnable {


    public void stuff2(int gold) {
        Player.buildGold = gold;
    }

    static int GetRoll(int gold, int sides) {
        Player.buildGold = gold;
        int total = 0;

        for (int i = 0; i < gold; i++) {
            int roll = (int) (Math.random() * sides) + 1;
            total += roll;
        }
        return total;
    }

    public void run() {
        ArrayList r = new ArrayList<>();
        int rollToAdd;
        for (int i = 0; i < 5; i++) {
            rollToAdd = GetRoll(Player.buildGold, 6) * 10;
            r.add(rollToAdd);
        }
        Player.rolledGold = (int) Collections.max(r);

    }
}