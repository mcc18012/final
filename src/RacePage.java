public class RacePage {

    static void Dwarf() {
        Player.constitution += 2;
        Player.traits.add("Darkvision");
        Player.traits.add("Dwarven Resilience");
        Player.traits.add("Dwarven Combat Training");
        Player.traits.add("Stonecunning");
    }

    static void Elf() {
        Player.dexterity += 2;
        Player.traits.add("Darkvision");
        Player.traits.add("Keen Senses");
        Player.traits.add("Fey Ancestry");
        Player.traits.add("Trance");
    }

    static void Gnome() {
        Player.intelligence += 2;
        Player.traits.add("Darkvision");
        Player.traits.add("Gnome Cunning");
    }

    static void Halfelf() {
        Player.dexterity += 1;
        Player.wisdom += 1;
        Player.charisma += 2;
        Player.traits.add("Darkvision");
        Player.traits.add("Fey Ancestry");
        Player.traits.add("Skill Versatility");
    }

    static void Halfling() {
        Player.dexterity += 2;
        Player.traits.add("Lucky");
        Player.traits.add("Brave");
        Player.traits.add("Halfling Nimbleness");
    }

    static void Halforc() {
        Player.strength += 2;
        Player.constitution += 1;
        Player.traits.add("Darkvision");
        Player.traits.add("Menacing");
        Player.traits.add("Relentless Endurance");
        Player.traits.add("Savage Attacks");
    }

    static void Human() {
        Player.strength += 1;
        Player.dexterity += 1;
        Player.constitution += 1;
        Player.intelligence += 1;
        Player.wisdom += 1;
        Player.charisma += 1;
        Player.traits.add("Extra Language");
    }
}
