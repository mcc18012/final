import org.json.simple.JSONArray;
import java.util.ArrayList;

public class Player {

    public static String name;
    public static String build;
    public static String race;
    public static ArrayList<String> traits = new ArrayList<String>();
    public static int hitPoints;
    public static int strength;
    public static int dexterity;
    public static int constitution;
    public static int intelligence;
    public static int wisdom;
    public static int charisma;
    public static int strengthMod;
    public static int dexterityMod;
    public static int constitutionMod;
    public static int intelligenceMod;
    public static int wisdomMod;
    public static int charismaMod;
    public static String armorProf;
    public static JSONArray weaponProf = new JSONArray();
    public static JSONArray toolProf = new JSONArray();
    public static JSONArray savingThrows = new JSONArray();
    public static JSONArray skillProf = new JSONArray();
    public static JSONArray specials = new JSONArray();
    public static int buildGold;
    public static int rolledGold;


    public void compile() throws Exception {
        Calculation c = new Calculation();
        c.raceCalc();
        c.calcStats();
        c.buildReader();
        IDK.stuff();

    }
};